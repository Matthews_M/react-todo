import React, { Component } from "react";
import "./todo.css";
export default class todo extends Component {
  state = {
    value: "",
    valarr: [],
    disable:true
  };
  handleVal = (event) => {
    const newvalarr = this.state.valarr;
    event.preventDefault();
    newvalarr.push(this.state.value);
    this.setState({
      valarr: newvalarr,
      value: "",
    });
  };
  handleChange = (event) => {
    this.setState({ value: event.target.value });
  };
  handleDel = (key) => {
    const newvalarr = [...this.state.valarr];
    newvalarr.splice(key, 1);
    console.log(newvalarr);
    this.setState({
      valarr: newvalarr,
    });
  };
  handleEdit = (event) => {
    const editInput = event.target;
    editInput.disabled=false;
    console.log("disable changed");
  };
  handleEditText = (key, event) => {
    const newvalarr = [...this.state.valarr];
    const newval = event.target.vaue;
    newvalarr[key] = newval;
    console.log(newvalarr);
  };
  render() {
    return (
      <div className="main-container">
        <div className="heading-container">
          <h2>Todo</h2>
          <form onSubmit={this.handleVal}>
            <input
              type="text"
              name=""
              id=""
              placeholder="Enter text ..."
              value={this.state.value}
              onChange={this.handleChange}
            />
          </form>
        </div>
        <ul>
          {this.state.valarr.map((data, index) => (
            <li key={index}>
              <input
                id="readOnlyIn"
                type="text"
                defaultValue={data}
                onChange={(event) => this.handleEditText(index, event)}
              />
              <i
                className="fas fa-pen-to-square"
                onClick={(event) => this.handleEdit( event)}
              ></i>
              <i
                className="fas fa-trash"
                onClick={(event) => this.handleDel(index)}
              ></i>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
